<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('add', 'CalculatorController@add');
Route::post('subtract', 'CalculatorController@sub');
Route::post('multiply', 'CalculatorController@multi');
Route::post('divide', 'CalculatorController@div');
Route::post('squareRoot', 'CalculatorController@square');
Route::post('save', 'CalculatorController@store');
Route::get('savedValue/{id}', 'CalculatorController@show');
Route::get('savedValue', 'CalculatorController@index');
Route::post('clear', 'CalculatorController@clear');
