
## The Information for API
- Host: `https://kathycalculatorapp.herokuapp.com/`
- Parameter for Addition/Subtraction/Multiplication/Division: `{"num1":  81, "num2": 9}`
    - Two field `num1,num2` are required and must be numeric
    - URI: 
        - `api/add`
        - `api/subtract`
        - `api/multiply`
        - `api/divide`
    - Method: `POST`
- Parameter for Square Root: `{"num1":  61}`
    - `num1` is required, must be numeric and greater than 0
    - URI: `api/squareRoot`
    - Method: `POST`
- Parameter for Save Value: `{"value": 12}`
    - `value` is required and must be numeric
    - URI: `api/save/{id}`
    - Method: `POST`
- Retrieve Value: 
    - URI: `api/savedValue/{id}`
    - Method: `GET`
- Parameter for Clear Value: `{"id": 1}`   
    - URI: `api/clear/{id}`
    - Method: `POST` 

## Project Structure
- The controller handle logic calculator and store function implemented on `app/Http/Controllers/CalculatorController.php`
- The model have named `Result`
- The test case of unit test on file `Feature/CalculatorTest.php`
