<?php

namespace Tests\Feature;

use Tests\TestCase;

class CalculatorTest extends TestCase
{
    public function testAdditionSuccessful()
    {
        $response = $this->call('POST', 'api/add',['num1'=>2,'num2'=>3]);
        $response->assertStatus(200);
        $this->assertEquals(5, $response->json('answer'));
    }

    public function testSubtractionSuccessful()
    {
        $response = $this->call('POST', 'api/subtract',['num1'=>10,'num2'=>3]);
        $response->assertStatus(200);
        $this->assertEquals(7, $response->json('answer'));
    }
    public function testMultiplicationSuccessful()
    {
        $response = $this->call('POST', 'api/multiply',['num1'=>2,'num2'=>3]);
        $response->assertStatus(200);
        $this->assertEquals(6, $response->json('answer'));
    }
    public function testDivisionSuccessful()
    {
        $response = $this->call('POST', 'api/divide',['num1'=>6,'num2'=>3]);
        $response->assertStatus(200);
        $this->assertEquals(2, $response->json('answer'));
    }
    public function testSquareRootSuccessful()
    {
        $response = $this->call('POST', 'api/squareRoot',['num1'=>4]);
        $response->assertStatus(200);
        $this->assertEquals(2, $response->json('answer'));
    }

    public function testValidationCalculatorParam()
    {
        $response = $this->call('POST', 'api/add',['num1'=>2,'num2'=>'ddd']);
        $response->assertStatus(422);
        $error = $response->json('errors')['num2'];
        $this->assertEquals('This param must be a numeric', $error[0]);
    }

    public function testMissingCalculatorParam()
    {
        $response = $this->call('POST', 'api/add',['num1'=>2]);
        $response->assertStatus(422);
        $error = $response->json('errors')['num2'];
        $this->assertEquals('Number 2 is required!', $error[0]);
    }

    public function testDivideWithInvalidValue()
    {
        $response = $this->call('POST', 'api/divide',['num1'=>2, 'num2'=>0]);
        $response->assertStatus(422);
        $error = $response->json('errors')['num2'];
        $this->assertEquals('Must greater than 0', $error[0]);
    }

    public function testSquareRootWithInvalidValue()
    {
        $response = $this->call('POST', 'api/squareRoot',['num1'=>0]);
        $response->assertStatus(422);
        $error = $response->json('errors')['num1'];
        $this->assertEquals('Must greater than 0', $error[0]);
    }

    public function testSaveValueSuccessful(){
        $response = $this->call('POST', 'api/save',['value'=>22]);
        $response->assertStatus(201);
        $this->assertEquals(true, $response->json('save'));
    }

    public function testSaveInvalidValue(){
        $response = $this->call('POST', 'api/save',['value'=>'ddd']);
        $response->assertStatus(422);
        $error = $response->json('errors')['value'];
        $this->assertEquals('Value must be a numeric', $error[0]);
    }

    public function testGetSavedValueSuccessful(){
        $addValue = $this->call('POST', 'api/save',['value'=>44]);
        $addValue->assertStatus(201);
        $response = $this->call('GET', 'api/savedValue/1');
        $response->assertStatus(200);
        $this->assertEquals("44", $response->json('value'));
        $this->assertCount(1, $response->json());
    }

    public function testGetNotExistSavedValue(){
        $addValue = $this->call('POST', 'api/save',['value'=>55]);
        $addValue->assertStatus(201);
        $response = $this->call('GET', 'api/savedValue/33');
        $response->assertStatus(404);
        $this->assertEquals("This value not exist", $response->json('error'));
    }

    public function testClearSavedValueSuccessful(){
        $addValue = $this->call('POST', 'api/save',['value'=>123]);
        $addValue->assertStatus(201);
        $getValue = $this->call('GET', 'api/savedValue/1');
        $getValue->assertStatus(200);
        $this->assertEquals("123", $getValue->json('value'));
        $clearValue = $this->call('POST', 'api/clear',['id'=>1]);
        $clearValue->assertStatus(204);
        $response = $this->call('GET', 'api/savedValue/1');
        $response->assertStatus(404);
        $this->assertEquals("This value not exist", $response->json('error'));
    }
}
