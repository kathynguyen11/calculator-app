<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class SquareRequest extends CalculatorRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        unset($rules['num2']);
        array_push($rules['num1'], 'gt:0');
        return $rules;
    }
    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(parent::messages(), [
            'num1.gt' => 'Must greater than 0'
        ]);
    }

    protected function failedValidation(Validator $validator)
    {
        parent::failedValidation($validator);
    }
}
