<?php

namespace App\Http\Controllers;

use App\Http\Requests\CalculatorRequest;
use App\Http\Requests\DivideRequest;
use App\Http\Requests\SquareRequest;
use App\Http\Requests\StoreRequest;
use App\Result;
use Illuminate\Http\Request;

class CalculatorController extends Controller
{
    public function add(CalculatorRequest $request)
    {
        $data = $request->validated();
        $num1 = $data['num1'];
        $num2 = $data['num2'];
        return response()->json(['answer' => $num1 + $num2])->setStatusCode(200);
    }

    public function sub(CalculatorRequest $request)
    {
        $data = $request->validated();
        $num1 = $data['num1'];
        $num2 = $data['num2'];
        return response()->json(['answer' => $num1 - $num2])->setStatusCode(200);
    }

    public function multi(CalculatorRequest $request)
    {
        $data = $request->validated();
        $num1 = $data['num1'];
        $num2 = $data['num2'];
        return response()->json(['answer' => $num1 * $num2])->setStatusCode(200);
    }

    public function div(DivideRequest $request)
    {
        $data = $request->validated();
        $num1 = $data['num1'];
        $num2 = $data['num2'];
        return response()->json(['answer' => $num1 / $num2])->setStatusCode(200);
    }

    public function square(SquareRequest $request)
    {
        $data = $request->validated();
        $num1 = $data['num1'];
        return response()->json(['answer' => sqrt($num1)])->setStatusCode(200);
    }

    public function store(StoreRequest $request)
    {
        try {
            $result = new Result();
            $result->value = $request->validated()['value'];
            $result->save();
            return response()->json(['save' => true], 201);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

    }

    public function show($id)
    {
        $result = Result::find($id);
        if ($result) {
            return response()->json(['value' => $result->value], 200);
        }
        return response()->json(['error' => 'This value not exist'], 404);
    }

    public function index()
    {
        return Result::all();
    }

    public function clear(Request $request)
    {
        Result::find($request->input('id'))->delete();

        return response()->json(['value' => null], 204);
    }
}
